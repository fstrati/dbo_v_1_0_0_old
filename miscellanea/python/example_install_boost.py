#!/usr/bin/env python
# encoding: utf-8

from argparse import ArgumentParser

import os
import subprocess
import tempfile


def main():
  parser = ArgumentParser()

  parser.add_argument('-source-dir',     required = True)
  parser.add_argument('-build-dir',      required = False,
                                         default  = '')
  parser.add_argument('-install-prefix', required = True)
  parser.add_argument('-toolchain',      required = True,
                                         choices  = ['borland',
                                                     'como',
                                                     'gcc',
                                                     'gcc-nocygwin',
                                                     'intel-win32',
                                                     'metrowerks',
                                                     'mingw',
                                                     'msvc',
                                                     'vc7',
                                                     'vc8',
                                                     'vc9',
                                                     'vc10',
                                                     'vc11'])
  parser.add_argument('-address-model',  required = True,
                                         choices  = ['32', '64'])
  parser.add_argument('-variant',        required = True,
                                         choices  = ['release', 'debug'])
  parser.add_argument('-c-flags',        required = False,
                                         default  = '')
  parser.add_argument('-cxx-flags',      required = False,
                                         default  = '')
  parser.add_argument('-link-flags',     required = False,
                                         default  = '')
  parser.add_argument('-verbose',        action   = 'store_true',
                                         default  = False)

  args = parser.parse_args()

  source_dir = os.path.realpath(args.source_dir)

  if args.build_dir:
    build_dir = os.path.realpath(args.build_dir)
  else:
    build_dir = tempfile.mkdtemp('.d', 'boost-build-')

  prefix = os.path.realpath(args.install_prefix)

  if not os.path.isdir(prefix):
    os.makedirs(prefix)

  toolset       = args.toolchain
  address_model = args.address_model
  variant       = args.variant
  cflags        = args.c_flags
  cxxflags      = args.cxx_flags
  linkflags     = args.link_flags
  verbose       = args.verbose

  architecture  = 'x86'
  link          = 'static'
  runtime_link  = 'static'
  threading     = 'multi'
  layout        = 'tagged'

  os.chdir(source_dir)

  if not os.path.isfile('b2.exe'):
    subprocess.call(('bootstrap.bat', toolset))
    os.remove('project-config.jam')

  b2_args = ['b2.exe']
  b2_args.append('-d')

  if verbose:
    b2_args.append('+2')
  else:
    b2_args.append('0')

  b2_args.append('--build-dir='   + build_dir)
  b2_args.append('--prefix='      + prefix)

  b2_args.append('toolset='       + toolset)
  b2_args.append('address-model=' + address_model)
  b2_args.append('variant='       + variant)

  if cflags:
    b2_args.append('cflags='      + cflags)

  if cxxflags:
    b2_args.append('cxxflags='    + cxxflags)

  if linkflags:
    b2_args.append('linkflags='   + linkflags)

  b2_args.append('architecture='  + architecture)
  b2_args.append('link='          + link)
  b2_args.append('runtime-link='  + runtime_link)
  b2_args.append('threading='     + threading)
  b2_args.append('--layout='      + layout)

  b2_args.append('install')

  subprocess.call(b2_args)


if __name__ == '__main__':
  main()
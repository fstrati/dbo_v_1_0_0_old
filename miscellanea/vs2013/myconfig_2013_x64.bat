@echo off

:: Set the path to VS2013
SET VISUAL_STUDIO_VC=C:\MSVC2013\VC
SET VISUAL_STUDIO_VC_BIN=C:\MSVC2013\VC\bin

:: Set CYGWIN_DIR to the location of CYGWIN root
SET CYGWIN_DIR=C:\cygwin64

:: Set the path
:: Add VC and CYGWIN
SET PATH=%PATH%;%VISUAL_STUDIO_VC%;%VISUAL_STUDIO_VC_BIN%;%CYGWIN_DIR%\bin;%CYGWIN_DIR%\usr\bin

:: Launch the set-up of vs2013 x64
cd %VISUAL_STUDIO_VC_BIN%\amd64
call .\vcvars64.bat

:: Set working directory
SET ZWORKS=C:\Works

:: Change command prompt path
cd %ZWORKS%

@echo on

cd %BOOST_ROOT%

SET PATH=%PATH%;%ZWORKS%\icu-install-x64\bin;%ZWORKS%\icu-install-x64\lib;

.\b2 -a -sICU_PATH=%ZWORKS%\icu-install-x64 variant=debug,release address-model=64 link=static,shared threading=single,multi runtime-link=shared --build-type=complete stage >x86_64_all.log 2>&1


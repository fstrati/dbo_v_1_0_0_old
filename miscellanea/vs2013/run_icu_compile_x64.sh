#!/bin/bash

ICU_ROOT=/cygdrive/c/Works/icu
export ICU_ROOT

ICU_ROOT_SRC=/cygdrive/c/Works/icu/source
export ICU_ROOT_SRC

# run for the x64 target
ICU_ROOT_INST=/cygdrive/c/Works/icu-install-x64
export ICU_ROOT_INST

mkdir $ICU_ROOT_INST
mkdir $ICU_ROOT_INST/lib

cd $ICU_ROOT_SRC

# 1. Run for the debug shared libs
./runConfigureICU --enable-debug --disable-release Cygwin/MSVC --enable-debug --disable-release --prefix=$ICU_ROOT_INST --with-data-packaging=static
make VERBOSE=1 >$ICU_ROOT_INST/icu_55.1.0-0_shd_dbg.log 2>&1
cp $ICU_ROOT_SRC/lib/*.exp $ICU_ROOT_INST/lib/
cp $ICU_ROOT_SRC/lib/*.pdb $ICU_ROOT_INST/lib/
make install
mv $ICU_ROOT_INST/lib/icud $ICU_ROOT_INST/lib/icud-shd
mv $ICU_ROOT_INST/share/icud $ICU_ROOT_INST/share/icud-shd
make distclean

# 2. Run for the debug static libs
./runConfigureICU --enable-debug --disable-release Cygwin/MSVC --enable-debug --disable-release --enable-static --disable-shared --prefix=$ICU_ROOT_INST --with-data-packaging=static
make VERBOSE=1 >$ICU_ROOT_INST/icu_55.1.0-0_stc_dbg.log 2>&1
make install
mv $ICU_ROOT_INST/lib/icud $ICU_ROOT_INST/lib/icud-stc
mv $ICU_ROOT_INST/share/icud $ICU_ROOT_INST/share/icud-stc
make distclean

# 3. Run for the release shared libs
./runConfigureICU Cygwin/MSVC --enable-release --disable-debug --prefix=$ICU_ROOT_INST --with-data-packaging=static
make VERBOSE=1 >$ICU_ROOT_INST/icu_55.1.0-0_shd_rel.log 2>&1
cp $ICU_ROOT_SRC/lib/*.exp $ICU_ROOT_INST/lib/
make install
mv $ICU_ROOT_INST/lib/icu $ICU_ROOT_INST/lib/icu-shd
mv $ICU_ROOT_INST/share/icu $ICU_ROOT_INST/share/icu-shd
make distclean

# 4. Run for the release static libs
./runConfigureICU Cygwin/MSVC --enable-release --disable-debug --enable-static --disable-shared --prefix=$ICU_ROOT_INST --with-data-packaging=static
make VERBOSE=1 >$ICU_ROOT_INST/icu_55.1.0-0_stc_rel.log 2>&1
make install
mv $ICU_ROOT_INST/lib/icu $ICU_ROOT_INST/lib/icu-stc
mv $ICU_ROOT_INST/share/icu $ICU_ROOT_INST/share/icu-stc
make distclean

# copy the datafile

mkdir $ICU_ROOT_INST/data
cp $ICU_ROOT_SRC/data/in/icudt55l.dat $ICU_ROOT_INST/data/

exit 0

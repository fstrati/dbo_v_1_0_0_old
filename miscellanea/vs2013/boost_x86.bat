@echo on

cd %BOOST_ROOT%

SET ORIG_PATH=%PATH%;

SET PATH=%ORIG_PATH%;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-static-release-vs2013\bin;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-static-release-vs2013\lib;

.\b2 --reconfigure -j4 --with-locale -sICU_PATH=C:\Users\fstrati\Desktop\Works\icu-dist-56.1-vs2013\icu-x86-static-release-vs2013^
     --user-config=C:\Users\fstrati\Desktop\Works\boost\user-config.jam --prefix=C:\Users\fstrati\Desktop\Works\boost-vs2013-install\boost-1.59.0-vs2013^
	 --build-dir=C:\Users\fstrati\Desktop\Works\boost-vs2013-build toolset=msvc-12.0 link=static runtime-link=static address-model=32 threading=multi variant=release^
	 --build-type=complete install

SET PATH=%ORIG_PATH%;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-static-debug-vs2013\bin;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-static-debug-vs2013\lib;

.\b2 --reconfigure -j4 --with-locale -sICU_PATH=C:\Users\fstrati\Desktop\Works\icu-dist-56.1-vs2013\icu-x86-static-debug-vs2013^
     --user-config=C:\Users\fstrati\Desktop\Works\boost\user-config.jam --prefix=C:\Users\fstrati\Desktop\Works\boost-vs2013-install\boost-1.59.0-vs2013^
	 --build-dir=C:\Users\fstrati\Desktop\Works\boost-vs2013-build toolset=msvc-12.0 link=static runtime-link=static address-model=32 threading=multi variant=debug^
	 --build-type=complete install

SET PATH=%ORIG_PATH%;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-shared-release-vs2013\bin;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-shared-release-vs2013\lib;

.\b2 --reconfigure -j4 --with-locale -sICU_PATH=C:\Users\fstrati\Desktop\Works\icu-dist-56.1-vs2013\icu-x86-shared-release-vs2013^
     --user-config=C:\Users\fstrati\Desktop\Works\boost\user-config.jam --prefix=C:\Users\fstrati\Desktop\Works\boost-vs2013-install\boost-1.59.0-vs2013^
	 --build-dir=C:\Users\fstrati\Desktop\Works\boost-vs2013-build toolset=msvc-12.0 link=shared runtime-link=shared address-model=32 threading=multi variant=release^
	 --build-type=complete install

SET PATH=%ORIG_PATH%;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-shared-debug-vs2013\bin;%ZWORKS%\icu-dist-56.1-vs2013\icu-x86-shared-debug-vs2013\lib;

.\b2 --reconfigure -j4 --with-locale -sICU_PATH=C:\Users\fstrati\Desktop\Works\icu-dist-56.1-vs2013\icu-x86-shared-debug-vs2013^
     --user-config=C:\Users\fstrati\Desktop\Works\boost\user-config.jam --prefix=C:\Users\fstrati\Desktop\Works\boost-vs2013-install\boost-1.59.0-vs2013^
	 --build-dir=C:\Users\fstrati\Desktop\Works\boost-vs2013-build toolset=msvc-12.0 link=shared runtime-link=shared address-model=32 threading=multi variant=debug^
	 --build-type=complete install


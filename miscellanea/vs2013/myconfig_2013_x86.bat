@echo off

:: Set working directory
SET ZWORKS=C:\Users\fstrati\Desktop\Works

:: Set the path to VS2013 - 12.0
SET VISUAL_STUDIO_VC=C:\MSVC2013\VC
SET VISUAL_STUDIO_VC_BIN=C:\MSVC2013\VC\bin

:: Set the path to MS SQL SERVER 12.0
SET MS_SQL_SERVER=C:\Program Files\Microsoft SQL Server\120\Tools;

:: Set PYTHON_DIR to the location of PYTHON root
SET PYTHON_DIR=C:\Python27

:: Set CMAKE_DIR to the location of CMAKE binaries
SET CMAKE_DIR=C:\Program Files (x86)\CMake\bin

:: Set GIT_MY_DIR to the location of GIT root
SET GIT_MY_DIR=C:\Program Files (x86)\Git\bin

:: Set QT_DIR to the location of QT root
SET QT_DIR=C:\Qt\5.5\msvc2013;
:: Set QT_CREATOR_DIR to the location of QT CREATOR root
SET QT_CREATOR_DIR=C:\Qt\Tools\QtCreator;

:: Set Eclipse directory
SET ECLIPSE_ROOT_DIR=C:\ZWorks\eclipse;

:: Set CYGWIN_DIR to the location of CYGWIN root
SET CYGWIN_DIR=C:\cygwin64

:: Set BOOST_ROOT to the location of BOOST root
SET BOOST_ROOT=%ZWORKS%\boost

:: Set the path, standard paths...
SET PATH=%SystemRoot%\system32;
SET PATH=%PATH%;%SystemRoot%;
SET PATH=%PATH%;%SystemRoot%\System32\Wbem;
SET PATH=%PATH%;%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\;
SET PATH=%PATH%;%MS_SQL_SERVER%\Binn\;
:: Set the path, additional paths...
SET PATH=%PATH%;%VISUAL_STUDIO_VC%;
SET PATH=%PATH%;%VISUAL_STUDIO_VC_BIN%;
:: SET PATH=%PATH%;%PYTHON_DIR%;
SET PATH=%PATH%;%CMAKE_DIR%;
SET PATH=%PATH%;%QT_DIR%;
SET PATH=%PATH%;%QT_DIR%\bin;
SET PATH=%PATH%;%QT_CREATOR_DIR%\bin;
SET PATH=%PATH%;%ECLIPSE_ROOT_DIR%;
SET PATH=%PATH%;"C:\Program Files\7-Zip"
:: SET PATH=%PATH%;%GIT_MY_DIR%;
SET PATH=%PATH%;%CYGWIN_DIR%\bin;
SET PATH=%PATH%;%CYGWIN_DIR%\usr\bin;

:: other paths

:: C:\csvn\bin\;
:: C:\csvn\Python25\;
:: C:\ProgramData\Oracle\Java\javapath;
:: C:\Python34\;
:: C:\Python34\Scripts;
:: C:\Program Files\Broadcom\Broadcom 802.11;
:: C:\Program Files (x86)\NVIDIA Corporation\PhysX\Common;
:: C:\Program Files (x86)\Intel\iCLS Client\;
:: C:\Program Files\Intel\iCLS Client\;
:: C:\Program Files\Hewlett-Packard\SimplePass\;
:: C:\Program Files\Intel\Intel(R) Management Engine Components\DAL;
:: C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\DAL;
:: C:\Program Files\Intel\Intel(R) Management Engine Components\IPT;
:: C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\IPT;
:: C:\Program Files (x86)\Windows Kits\8.1\Windows Performance Toolkit\;
:: C:\Program Files\Microsoft SQL Server\110\Tools\Binn\;
:: C:\Program Files (x86)\Microsoft SDKs\TypeScript\1.0\;
:: C:\Program Files\Microsoft\Web Platform Installer\;
:: C:\Program Files (x86)\Skype\Phone\;
:: C:\Program Files\TortoiseSVN\bin

:: Launch the set-up of vs2013 x86
call %VISUAL_STUDIO_VC_BIN%\vcvars32.bat

:: Change command prompt path
cd %ZWORKS%


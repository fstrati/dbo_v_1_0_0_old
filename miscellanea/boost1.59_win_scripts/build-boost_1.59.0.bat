@echo off
SETLOCAL EnableExtensions EnableDelayedExpansion
::
:: ========================================================================================================
:: ==== Boost Librares compilation with Boost.Python and Localization
:: ========================================================================================================
:: 
:: This script is still under development and you possibly require
:: to configure it (see <CONFIGURATION> section)
:: 
::    Author: stathis <stathis@npcglib.org>
:: Copyright: 2012-2015
::  Revision: $Id: build-boost_1.59.0.bat 4783 2015-08-27 20:54:30Z stathis $
::
:: Changelog:
::
:: - threading is always set to "multi", "single" is not supported by Boost.Locale.
:: - NO_ZLIB or NO_COMPRESSION is broken as of 1.57 (see https://svn.boost.org/trac/boost/ticket/9156)
:: - Changed the format of the directory naming. 
::   It is now: boost-X.XX.X-vsXXXX for 32-bit
::         and: boost64-X.XX.X-vsXXXX for 64-bit
:: - A log file is generated for each build flavor.
:: - The VS toolset was hardcoded. It is now detected properly.
::
:: ========================================================================================================
:: ==== <CONFIGURATION>
:: ========================================================================================================
:: 
:: Set the version of Visual Studio. This will just add a suffix to the string
:: of your directories to avoid mixing them up.
SET VS_VERSION=vs2013

:: Set this to the directory that contains vcvarsall.bat file of the 
:: VC Visual Studio version you want to use for building ICU.
:: SET VISUAL_STUDIO_VC=D:\dev\!VS_VERSION!\VC

:: Set this to the name of the project
SET BUILD_PROJECT=boost

:: Set this to the version of ICU you are building
SET PROJECT_VERSION=1.59.0

:: Set this to the Windows directory that contains the toplevel ICU 
:: distribution (one level up from the icu/source directory)
SET PROJECT_SRC_DIR=%BOOST_ROOT%

:: This is the directory the library will be installed once it is build (make install)
SET DESTPATH=%ZWORKS%\boost-install

SET BOOST_BUILD_DIR=!PROJECT_SRC_DIR!-!VS_VERSION!-build

:: Set CYGWIN_DIR to the location of your Cygwin root ( one level up from /bin )
:: I use Cygwin Portable (http://symbiosoft.net/projects/cygwin-portable)
:: Make sure you install the following packages:
::  p7zip, md5sum, patch
:: SET CYGWIN_DIR=D:\PortableApps\CygwinPortable\App\Cygwin

:: Directory of precompiled ICU to use for Boost.Locale
:: As of Boost 1.59.0 there is NO support in Boost for linking against ICU static,
:: it always links to ICU shared. I reported this here: https://svn.boost.org/trac/boost/ticket/9685
SET ICU_SHARED=%ZWORKS%\icu-dist-56.1-!VS_VERSION!\icu-56.1-vs2013
SET ICU_STATIC=%ZWORKS%\icu-dist-56.1-!VS_VERSION!\icu-56.1-vs2013

:: 
:: ========================================================================================================
:: ==== </CONFIGURATION>
:: ========================================================================================================
:: 
SET _DEBUG=0

::
:: ========================================================================================================
::

@mkdir !BOOST_BUILD_DIR! 2>nul

IF DEFINED CYGWIN_DIR (
	IF NOT EXIST "!CYGWIN_DIR!" (
		ECHO.
		CALL :exitB "ERROR: Cygwin directory !CYGWIN_DIR! does not exist. You need a valid Cygwin installation to use this script. Aborting."
		GOTO :eof
	)
	
	SET PATH=!PATH!;!CYGWIN_DIR!\bin
)

IF NOT EXIST "!PROJECT_SRC_DIR!" (
	ECHO.
	CALL :exitB "ERROR: Source directory !PROJECT_SRC_DIR! does not exist or does not contain the !BUILD_PROJECT! sources. Aborting."
	GOTO :eof
)

if "%1" == "" goto usage

if /i %1 == build (
	goto buildall
) else if /i %1 == package (
	goto createPackage
	goto checksumPackage
) else if /i %1 == checksum (
	goto checksumPackage
) else (
	goto usage
)

ENDLOCAL
@exit /B 0


:usage
echo:
echo Error in script usage. The correct usage is:
echo     %0 [ build ^| package ^| checksum ]
goto :eof


:checksumPackage

for %%a in (x86 x64) do (

	echo:
	
	SET PREFIX_WIN=!DESTPATH!\!BUILD_PROJECT!
	
	if /i "%%a" == "x86" (
		SET BITS=32
	) else (
		SET BITS=64
		SET PREFIX_WIN=!PREFIX_WIN!!BITS!
	)
	
	SET PREFIX_WIN=!PREFIX_WIN!-!PROJECT_VERSION!-!VS_VERSION!
	
	pushd !BOOST_BUILD_DIR!
		
	set COMPRESSED_FILE=!BUILD_PROJECT!-!PROJECT_VERSION!-%%a-!VS_VERSION!
	
	IF EXIST !COMPRESSED_FILE!.7z (
		
		for %%I in (!COMPRESSED_FILE!.7z) do (
			SET /A _fsize=%%~zI / 1024 / 1024
		)
		
		SET MD5SUM_CMD=md5sum !COMPRESSED_FILE!.7z
		echo        md5sum: !MD5SUM_CMD!
		if not %_DEBUG% == 1 (
			!MD5SUM_CMD! 1> !COMPRESSED_FILE!.md5
		)
		
		echo Generated md5sum for !COMPRESSED_FILE!.md5 [!_fsize!MB]
	)
	
	popd

)
goto :eof


:createPackage

for %%a in (x86 x64) do (

	echo:
	
	SET PREFIX_WIN=!DESTPATH!\!BUILD_PROJECT!
	
	if /i "%%a" == "x86" (
		SET BITS=32
	) else (
		SET BITS=64
		SET PREFIX_WIN=!PREFIX_WIN!!BITS!
	)
	
	SET PREFIX_WIN=!PREFIX_WIN!-!PROJECT_VERSION!-!VS_VERSION!
	
	set COMPRESSED_FILE=!BOOST_BUILD_DIR!\!BUILD_PROJECT!-!PROJECT_VERSION!-%%a-!VS_VERSION!
	
	:: -mx9 ultimate compression	
	SET COMPRESS_CMD=7z a -r0 -mx9 !COMPRESSED_FILE!.7z !PREFIX_WIN!\
	
	echo Compressing in: !COMPRESSED_FILE!.7z ... Please Wait!
	echo        Command: !COMPRESS_CMD!
	
	if not %_DEBUG% == 1 (
		!COMPRESS_CMD! 
		rem 1> nul
	)
	
)
	
goto :eof



:buildall

pushd !PROJECT_SRC_DIR!

call bootstrap.bat

for %%a in (x64 x86) do (

	SET B_CMD=b2 --reconfigure -j4
	SET PREFIX_WIN=!DESTPATH!\!BUILD_PROJECT!
	
	if /i "%%a" == "x86" (
		SET BITS=32
		SET USER_CONFIG=%~dp0user-config.jam
	) else (
		SET BITS=64
		SET USER_CONFIG=%~dp0user-config64.jam
		SET PREFIX_WIN=!PREFIX_WIN!!BITS!
	)
	
	SET THREADING_MODEL=multi
	
	SET PREFIX_WIN=!PREFIX_WIN!-!PROJECT_VERSION!-!VS_VERSION!
	
	echo:
	if not exist "!VISUAL_STUDIO_VC!\vcvarsall.bat" (
		call :exitB "!VISUAL_STUDIO_VC!\vcvarsall.bat does not exist."
		goto :eof
	)
	
	SETLOCAL EnableExtensions EnableDelayedExpansion

	call "!VISUAL_STUDIO_VC!\vcvarsall.bat" %%a
	echo:
	
	rem ==================== 

	SET TOOLSET=msvc

	IF "!VS_VERSION!" == "vs2015" (
		SET TOOLSET=!TOOLSET!-14.0
	)
	
	IF "!VS_VERSION!" == "vs2013" (
		SET TOOLSET=!TOOLSET!-12.0
	)
	
	IF "!VS_VERSION!" == "vs2012" (
		SET TOOLSET=!TOOLSET!-11.0
	)

	IF "!VS_VERSION!" == "vs2010" (
		SET TOOLSET=!TOOLSET!-10.0
	)

	IF "!VS_VERSION!" == "vs2008" (
		SET TOOLSET=!TOOLSET!-9.0
	)

	rem ==================== 

	for %%l in (static shared) do (
		
		if /i "%%l" == "static" (
			SET ICU_LIB_PATH=!ICU_STATIC!
		) else (
			SET ICU_LIB_PATH=!ICU_SHARED!
		)
		
		SET ICU_PATH =!ICU_LIB_PATH!\data

		for %%r in (static shared) do (
		
			for %%b in (release debug) do (
			
				SET BTDIR=!BUILD_PROJECT!-%%b-%%l-win!BITS!
				
				SET COMMON_OPTIONS=-sICU_PATH=!ICU_LIB_PATH!
				rem SET COMMON_OPTIONS=!COMMON_OPTIONS! -sNO_COMPRESSION=1
				rem SET COMMON_OPTIONS=!COMMON_OPTIONS! -sNO_BZIP2=1
				rem SET COMMON_OPTIONS=!COMMON_OPTIONS! -sNO_ZLIB=1

				SET COMMON_OPTIONS=!COMMON_OPTIONS! --user-config=!USER_CONFIG! --prefix=!PREFIX_WIN! --build-dir=!BOOST_BUILD_DIR! toolset=!TOOLSET! link=%%l runtime-link=%%r address-model=!BITS! threading=!THREADING_MODEL! variant=%%b --build-type=complete install
				
				SET LOG_FILE=!BOOST_BUILD_DIR!\!BUILD_PROJECT!!BITS!-!PROJECT_VERSION!-%%b-%%l-!VS_VERSION!.log
				
					echo ============================================================================================================ >> !LOG_FILE!
					echo Command: !B_CMD! --without-mpi !COMMON_OPTIONS!
					echo Command: !B_CMD! --without-mpi !COMMON_OPTIONS! >> !LOG_FILE!
					call !B_CMD! --without-mpi !COMMON_OPTIONS! 2>&1 >> !LOG_FILE!
					echo ============================================================================================================ >> !LOG_FILE!
					
					echo ============================================================================================================ >> !LOG_FILE!
					echo Command: !B_CMD! --with-python !COMMON_OPTIONS!
					echo Command: !B_CMD! --with-python !COMMON_OPTIONS! >> !LOG_FILE!
					call !B_CMD! --with-python !COMMON_OPTIONS! 2>&1 >> !LOG_FILE!
					echo ============================================================================================================ >> !LOG_FILE!
				
			)
		)
	)
	
	ENDLOCAL
)

popd
goto :eof

rem ========================================================================================================

:: %1 an error message
:exitB
echo:
echo Error: %1
echo:
echo Contact stathis@npcglib.org
@exit /B 0

#!/bin/bash
# projects cloning
git clone https://github.com/miloyip/rapidjson.git >rapidjson_clone.log

git clone https://github.com/jbeder/yaml-cpp.git >yaml-cpp_clone.log

git clone https://github.com/open-source-parsers/jsoncpp.git >jsoncpp_clone.log

git clone https://github.com/pocoproject/poco.git >poco_clone.log

git clone --recursive https://github.com/boostorg/boost.git boost >boost_clone.log

git clone https://github.com/SOCI/soci.git >soci_clone.log

git clone https://fstrati@bitbucket.org/fstrati/dbo.git >dbo_clone.log

git clone https://github.com/fstrati/dbo_old.git >dbo_old_clone.log

#mkdir ./icu
#cd ./icu
#svn co http://source.icu-project.org/repos/icu/icu/trunk
#cd ..

svn export http://source.icu-project.org/repos/icu/icu/tags/release-56-1 icu-svn-56.1 --native-eol LF

mkdir ./xerces
cd ./xerces
svn co http://svn.apache.org/repos/asf/xerces/c/trunk
cd ..





:: remove original directory
RD /S /Q %ZWORKS%\cmake_build\VTK_build_x64
RD /S /Q C:\ZWorks\VTK_64

:: make new original directory
mkdir %ZWORKS%\cmake_build\VTK_build_x64
mkdir C:\ZWorks\VTK_64

:: run there
cd %ZWORKS%\cmake_build\VTK_build_x64

:: run cmake
cmake.exe ^
      -G "Visual Studio 12 2013 Win64" ^
      -DCMAKE_INSTALL_PREFIX:PATH="C:\ZWorks\VTK_64" ^
      -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;MinSizeRel;RelWithDebInfo" ^
      -DBUILD_SHARED_LIBS:BOOL="1" ^
      -DBUILD_DOCUMENTATION:BOOL="1" ^
      -DBUILD_EXAMPLES:BOOL="1" ^
	  -DVTK_ALL_NEW_OBJECT_FACTORY:BOOL="1" ^
      -DVTK_BUILD_ALL_MODULES:BOOL="0" ^
      -DVTK_BUILD_ALL_MODULES_FOR_TESTS:BOOL="0" ^
      -DVTK_MAKE_INSTANTIATORS:BOOL="1" ^
      -DPYTHON_EXECUTABLE:FILEPATH="C:\Python27_64\python.exe" ^
      -DVTK_WRAP_PYTHON:BOOL="1" ^
	  -DVTK_QT_VERSION:STRING="5" ^
      -DVTK_Group_Qt:BOOL="1" ^
      -DQt5_DIR:PATH="C:\Qt\5.5\msvc2013_64\lib\cmake\Qt5" ^
      -DQT_QMAKE_EXECUTABLE:FILEPATH="C:\Qt\5.5\msvc2013_64\bin\qmake.exe" ^
      -DModule_vtkGUISupportQt:BOOL="1" ^
	  -DModule_vtkGUISupportQtOpenGL:BOOL="1" ^
      -DModule_vtkGUISupportQtSQL:BOOL="1" ^
      -DModule_vtkGUISupportQtWebkit:BOOL="1" ^
      -DVTK_Group_Imaging:BOOL="1" ^
      -DVTK_Group_Views:BOOL="1" ^
	  -DVTK_Group_Web:BOOL="1" ^
      -DModule_vtkGUISupportMFC:BOOL="1" ^
      -DCYGWIN_INSTALL_PATH:PATH="C:\cygwin64" ^
      -DDOXYGEN_DOT_EXECUTABLE:FILEPATH="C:\cygwin64\bin\doxygen.exe" ^
      -DDOXYGEN_EXECUTABLE:FILEPATH="C:\cygwin64\bin\doxygen.exe" ^
      %ZWORKS%\VTK

cd %ZWORKS%\VTK

# Collection of common functionality for CMake

# Find the Microsoft mc.exe message compiler
#
#  CMAKE_MC_COMPILER - where to find mc.exe
if(WIN32)
if(MSVC)
  # cmake has CMAKE_RC_COMPILER, but no message compiler
  if ("${CMAKE_GENERATOR}" MATCHES "Visual Studio")
    # this path is only present for 2008+, but we currently require PATH to
    # be set up anyway
    get_filename_component(sdk_dir "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SDKs\\Windows;CurrentInstallFolder]" REALPATH)
    get_filename_component(kit_dir "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows Kits\\Installed Roots;KitsRoot]" REALPATH)
    get_filename_component(kit81_dir "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows Kits\\Installed Roots;KitsRoot81]" REALPATH)
    if (X64)
      set(sdk_bindir "${sdk_dir}/bin/x64")
      set(kit_bindir "${kit_dir}/bin/x64")
      set(kit81_bindir "${kit81_dir}/bin/x64")
    else (X64)
      set(sdk_bindir "${sdk_dir}/bin")
      set(kit_bindir "${kit_dir}/bin/x86")
      set(kit81_bindir "${kit81_dir}/bin/x86")
    endif (X64)
  endif ()
  find_program(CMAKE_MC_COMPILER mc.exe HINTS "${sdk_bindir}" "${kit_bindir}" "${kit81_bindir}" DOC "path to message compiler")
  if (NOT CMAKE_MC_COMPILER)
    message(FATAL_ERROR "message compiler not found: required to build")
  endif (NOT CMAKE_MC_COMPILER)
  message(STATUS "Found message compiler: ${CMAKE_MC_COMPILER}")
  mark_as_advanced(CMAKE_MC_COMPILER)
endif(MSVC)
endif(WIN32)

#===============================================================================
# Macros for Source file management
#
#  DBO_SOURCES_PLAT - Adds a list of files to the sources of a components
#    Usage: DBO_SOURCES_PLAT( out name platform sources )
#      INPUT:
#           out:            the variable the sources are added to
#           name:           the name of the components
#           platform:       the platform this sources are for (ON = All, OFF = None, WIN32, UNIX ...)
#           sources:        a list of files to add to ${out}
#    Example: DBO_SOURCES_PLAT( SRCS dbo_core ON src/generic_object.cpp )
#
#  DBO_SOURCES - Like DBO_SOURCES_PLAT with platform = ON (Built on all platforms)
#    Usage: DBO_SOURCES( out name sources )
#    Example: DBO_SOURCES( SRCS dbo_core src/generic_object.cpp )
#
#  DBO_HEADERS - Adds a list of files to the headers of a components
#    Usage: DBO_HEADERS( out name headers )
#      INPUT:
#           out:            the variable the headers are added to
#           name:           the name of the components
#           headers:        a list of files to add to HDRS
#    Example: DBO_HEADERS( HDRS dbo_core include/dbo/generic_object.h )
#
#  DBO_HEADERS_AUTO - Like DBO_HEADERS but the name is read from the file header // Package: X
#    Usage: DBO_HEADERS_AUTO( out headers)
#    Example: DBO_HEADERS_AUTO( HDRS src/generic_object.cpp )
#

macro(DBO_SOURCES_PLAT out name platform)
    source_group("${name}\\Source Files" FILES ${ARGN})
    list(APPEND ${out} ${ARGN})
    if(NOT (${platform}))
        set_source_files_properties(${ARGN} PROPERTIES HEADER_FILE_ONLY TRUE)
    endif()
endmacro()

macro(DBO_SOURCES_AUTO out name)
    DBO_SOURCES_PLAT( ${out} ${name} ON ${ARGN} )
endmacro()

macro(DBO_HEADERS_AUTO out name)
    foreach( f ${ARGN})
        get_filename_component(fname ${f} NAME)
		message(STATUS "Header: ${name} ${f}")
		DBO_HEADERS( ${out} ${name} ${f} )
    endforeach()
endmacro()

macro(DBO_HEADERS out name)
    set_source_files_properties(${ARGN} PROPERTIES HEADER_FILE_ONLY TRUE)
    source_group("${name}\\Header Files" FILES ${ARGN})
    list(APPEND ${out} ${ARGN})
endmacro()



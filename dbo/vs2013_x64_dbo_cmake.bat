@echo on

SETLOCAL EnableExtensions EnableDelayedExpansion

SET ORIGINAL_PATH=%PATH%

SET PATH=%PATH%;%QTDIR%;
SET PATH=%PATH%;%QTDIR%\bin;
SET PATH=%PATH%;%QTDIR%\lib;

:: backup existing build directory
FOR /F "delims=" %%i IN ('C:\CYGWIN64\bin\date +%%Y%%m%%dT%%H%%M%%S') DO set DATE_NOW=%%i

SET BUILD_DIR=%ZWORKS%\cmake_build\dbo_build_vs2013_64
SET BUILD_DIR_BKP=%BUILD_DIR%_bkp_%DATE_NOW%

MOVE /Y %BUILD_DIR% %BUILD_DIR_BKP%

:: remove original directory
RD /S /Q %BUILD_DIR%

:: make new build directory
MKDIR %BUILD_DIR%

:: run there
cd %BUILD_DIR%

:: run cmake
cmake.exe ^
      -G "Visual Studio 12 2013 Win64" ^
      -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;MinSizeRel;RelWithDebInfo" ^
      -DPYTHON_EXECUTABLE:FILEPATH="C:\Python27_64\python.exe" ^
      -DDOXYGEN_DOT_EXECUTABLE:FILEPATH="C:\cygwin64\bin\doxygen.exe" ^
      -DDOXYGEN_EXECUTABLE:FILEPATH="C:\cygwin64\bin\doxygen.exe" ^
      -DDBO_STATIC:BOOL="1" ^
	  -DDBO_MT:BOOL="0" ^
	  -DBoost_INCLUDE_DIR:PATH="C:\Users\fstrati\Desktop\Works\boost-install\boost64-1.59.0-vs2013\include\boost-1_59" ^
	  -DBoost_LIBRARY_DIR:PATH="C:\Users\fstrati\Desktop\Works\boost-install\boost64-1.59.0-vs2013\lib" ^
      C:\Users\fstrati\Desktop\Works\dbo_v_1_0_0\dbo

cd %ZWORKS%\dbo_v_1_0_0\dbo

ENDLOCAL

@exit /B 0



@echo off

:: Set working directory
SET ZWORKS=C:\Users\fstrati\Desktop\Works

:: Set the path to VS2013 - 12.0
SET VISUAL_STUDIO_VC=C:\MSVC2013\VC
SET VISUAL_STUDIO_VC_BIN=C:\MSVC2013\VC\bin

:: Set the path to MS SQL SERVER 12.0
SET MS_SQL_SERVER=C:\Program Files\Microsoft SQL Server\120\Tools

:: Set PYTHON_DIR to the location of PYTHON root
SET PYTHON_DIR=C:\Python27
SET PYTHON64_DIR=C:\Python27_64

:: Set CMAKE_DIR to the location of CMAKE binaries
SET CMAKE_DIR=C:\Program Files (x86)\CMake\bin

:: Set GIT_MY_DIR to the location of GIT root
SET GIT_MY_DIR=C:\Program Files (x86)\Git\bin

:: Set QTDIR to the location of QT root
SET QTDIR=C:\Qt\5.5\msvc2013_64
:: Set QT_CREATOR_DIR to the location of QT CREATOR root
SET QT_CREATOR_DIR=C:\Qt\Tools\QtCreator

:: Set Eclipse directory
SET ECLIPSE_ROOT_DIR=C:\ZWorks\eclipse

:: Set CYGWIN_DIR to the location of CYGWIN root
SET CYGWIN_DIR=C:\cygwin64

:: Set MINGW_DIR to the location of MINGW root
SET MINGW_DIR=C:\TDM-GCC-64

:: Set the path, standard paths...
SET PATH=%SystemRoot%\system32;
SET PATH=%PATH%;%SystemRoot%;
SET PATH=%PATH%;%SystemRoot%\System32\Wbem;
SET PATH=%PATH%;%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\;
SET PATH=%PATH%;%MS_SQL_SERVER%\Binn\;
:: Set the path, additional paths...
:: SET PATH=%PATH%;%VISUAL_STUDIO_VC%;
:: SET PATH=%PATH%;%VISUAL_STUDIO_VC_BIN%;
:: SET PATH=%PATH%;%PYTHON_DIR%;
SET PATH=%PATH%;%CMAKE_DIR%;
:: SET PATH=%PATH%;%QTDIR%;
:: SET PATH=%PATH%;%QTDIR%\bin;
:: SET PATH=%PATH%;%QTDIR%\lib;
:: SET PATH=%PATH%;%QT_CREATOR_DIR%\bin;
:: SET PATH=%PATH%;%ECLIPSE_ROOT_DIR%;
:: SET PATH=%PATH%;"C:\Program Files\7-Zip"
:: SET PATH=%PATH%;%CYGWIN_DIR%\bin;
:: SET PATH=%PATH%;%CYGWIN_DIR%\usr\bin;

:: Change command prompt path
cd %ZWORKS%\dbo_v_1_0_0\dbo


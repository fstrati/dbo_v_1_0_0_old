/*
*  The Dynamic Business Objects Library (DBO acronym) is licensed under the terms
*  of the Boost Software License - Version 1.0 - August 17th, 2003 (see below).
*
*  Copyright (c): Federico Strati (private person) - 10th September 2015
*        Address: Via Tobagi 21 - 20143 - Milano - Italy
*         E-mail: fede[dot]strati<at>gmail[dot]com
*
*/
/*
* Boost Software License - Version 1.0 - August 17th, 2003
*
* Permission is hereby granted, free of charge, to any person or organization
* obtaining a copy of the software and accompanying documentation covered by
* this license (the "Software") to use, reproduce, display, distribute,
* execute, and transmit the Software, and to prepare derivative works of the
* Software, and to permit third-parties to whom the Software is furnished to
* do so, all subject to the following:
*
* The copyright notices in the Software and this entire statement, including
* the above license grant, this restriction and the following disclaimer,
* must be included in all copies of the Software, in whole or in part, and
* all derivative works of the Software, unless such copies or derivative
* works are solely in the form of machine-executable object code generated by
* a source language processor.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

//<! Generic Object Reader Stream File Reader

#include "../include/dbo/memory_binary_reader.h"

#include <iostream>
#include <sstream>

NAMESPACE_DBO_OPEN

CMemoryObjectBinReader::CMemoryObjectBinReader(const dbo_byte_t* const pMemoryArea, const size_t bSize)
   : _size(DBO_INT_ZERO)
{
   _pdevice = DBO_NEW boost::iostreams::basic_array_source<dbo_char_t>(pMemoryArea, bSize);
   _pistream = DBO_NEW boost::iostreams::stream<boost::iostreams::basic_array_source<dbo_char_t> >(*_pdevice);
   _bSwap = this->readBinaryFlip();
   this->readBinaryLibraryVersion();
}

CMemoryObjectBinReader::~CMemoryObjectBinReader()
{
   DBO_DELETE _pistream;
   DBO_DELETE _pdevice;
}

dbo_bool_t CMemoryObjectBinReader::readBinaryFlip()
{
   _bSwap = false;
   dbo_kind_t theFileStart = DBO_KIND_NULL;
   _pistream->read(reinterpret_cast<dbo_byte_t*> (&theFileStart), sizeof(dbo_kind_t));
   if (!_pistream->good())
   {
      DBO_ERROR_THROW_TMP;
   }
   _size += sizeof(dbo_kind_t);
   if ((theFileStart != DBO_KIND_LITTLE_ENDIAN) && (theFileStart != DBO_KIND_BIG_ENDIAN))
   {
      DBO_ERROR_THROW_TMP;
   }
   if (ByteOrder::isLittleEndian())
   {
      if (DBO_KIND_BIG_ENDIAN == theFileStart)
      {
         _bSwap = true;
      }
   }
   if (ByteOrder::isBigEndian())
   {
      if (DBO_KIND_LITTLE_ENDIAN == theFileStart)
      {
         _bSwap = true;
      }
   }
   return _bSwap;
}

void CMemoryObjectBinReader::readBinaryLibraryVersion()
{
   _libraryVersion = dbo_string_t("undefined");
   _pistream->read(reinterpret_cast<dbo_byte_t*> (&_version), sizeof(dbo_kind_t));
   if (!_pistream->good())
   {
      DBO_ERROR_THROW_TMP;
   }
   _size += sizeof(dbo_kind_t);
   _pistream->read(reinterpret_cast<dbo_byte_t*> (&_major), sizeof(dbo_kind_t));
   if (!_pistream->good())
   {
      DBO_ERROR_THROW_TMP;
   }
   _size += sizeof(dbo_kind_t);
   _pistream->read(reinterpret_cast<dbo_byte_t*> (&_minor), sizeof(dbo_kind_t));
   if (!_pistream->good())
   {
      DBO_ERROR_THROW_TMP;
   }
   _size += sizeof(dbo_kind_t);

   std::ostringstream oss;
   oss << "dbo_" << _version << "_" << _major << "_" << _minor;
   _libraryVersion = oss.str();
}

dbo_string_t CMemoryObjectBinReader::getBinaryLibraryVersion(dbo_kind_t& version, dbo_kind_t& major, dbo_kind_t& minor)
{
   version = _version;
   major   = _major;
   minor   = _minor;
   return _libraryVersion;
}

dbo_bool_t CMemoryObjectBinReader::is_swap() const
{
   return _bSwap;
}

CMemoryObjectBinReader& CMemoryObjectBinReader::operator >> (CGenericObject& value)
{
   if (!_pistream->eof())
   {
      dbo_kind_t theObjectStart = DBO_KIND_NULL;
      _pistream->read(reinterpret_cast<dbo_byte_t*> (&theObjectStart), sizeof(dbo_kind_t));
      _size += sizeof(dbo_kind_t);
      if (!_pistream->good() || _pistream->eof())
      {
         value.setValid(false);
         return *this;
      }
      if (DBO_KIND_OBJECT_START == theObjectStart)
      {
         size_t bSize;
         _pistream->read(reinterpret_cast<dbo_byte_t*> (&bSize), sizeof(size_t));
         bSize = ByteOrder::swapBytes(bSize, this->is_swap());
         _size += sizeof(size_t);
         if (!_pistream->eof())
         {
            // seek read characters backward
            _pistream->seekg(-1 * (sizeof(size_t) + sizeof(dbo_kind_t)), std::ios::cur);
            _size -= (sizeof(size_t) + sizeof(dbo_kind_t));
            // read the object
            dbo_byte_t* pObject = static_cast<dbo_byte_t*> (DBO_MALLOC(bSize));
            if (DBO_VALIDP(pObject))
            {
               _pistream->read(pObject, bSize);
               _size += bSize;
               value.setObjectBinData(bSize, pObject, this->is_swap());
               DBO_FREE(pObject);
            }
            else
            {
               value.setValid(false);
               return *this;
            }
         }
         else
         {
            value.setValid(false);
            return *this;
         }
      }
      else
      {
         value.setValid(false);
         return *this;
      }
   }
   return *this;
}

CMemoryObjectBinReader& CMemoryObjectBinReader::operator >> (CGenericObject* value)
{
   if (DBO_VALIDP(value))
   {
      *this >> (*value);
   }
   return *this;
}

CMemoryObjectBinReader& CMemoryObjectBinReader::operator >> (std::vector<CGenericObject>& value)
{
   while (!_pistream->eof())
   {
      CGenericObject theObject;
      *this >> theObject;
      if (theObject.isValid())
      {
         value.push_back(theObject);
      }
   }
   return *this;
}

CMemoryObjectBinReader& CMemoryObjectBinReader::operator >> (std::vector<CGenericObject*>& value)
{
   while (!_pistream->eof())
   {
      CGenericObject* pObject = DBO_NEW CGenericObject();
      *this >> (*pObject);
      if (pObject->isValid())
      {
         value.push_back(pObject);
      }
   }
   return *this;
}

CMemoryObjectBinReader& CMemoryObjectBinReader::operator >> (boost::shared_ptr<CGenericObject> value)
{
   if ( value )
   {
      *this >> (*value);
   }
   return *this;
}

CMemoryObjectBinReader& CMemoryObjectBinReader::operator >> (std::vector<boost::shared_ptr<CGenericObject> >& value)
{
   while (!_pistream->eof())
   {
      boost::shared_ptr<CGenericObject> pObject = boost::make_shared<CGenericObject>();
      *this >> (*pObject);
      if (pObject->isValid())
      {
         value.push_back(pObject);
      }
   }
   return *this;
}

NAMESPACE_DBO_CLOSE



#!/bin/bash

DATE_NOW=$(date +%Y%m%dT%H%M%S)

BUILD_DIR=${HOME}/cmake_build/dbo_build_eclipse_msys2_x64
BUILD_DIR_BKP=${BUILD_DIR}_bkp_${DATE_NOW}

# backup original directory
mv $BUILD_DIR $BUILD_DIR_BKP

# remove original directory
rm -rf $BUILD_DIR

# make new build directory
mkdir -p $BUILD_DIR

# run there
cd $BUILD_DIR

# run cmake
cmake \
      -G "Eclipse CDT4 - Unix Makefiles" \
      -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;MinSizeRel;RelWithDebInfo" \
      -DDBO_STATIC:BOOL="1" \
      -DDBO_MT:BOOL="0" \
      ${HOME}/dbo_v_1_0_0/dbo

cd ${HOME}/dbo_v_1_0_0/dbo

exit 0




/*
*  The Dynamic Business Objects Library (DBO acronym) is licensed under the terms
*  of the Boost Software License - Version 1.0 - August 17th, 2003 (see below).
*
*  Copyright (c): Federico Strati (private person) - 10th September 2015
*        Address: Via Tobagi 21 - 20143 - Milano - Italy
*         E-mail: fede[dot]strati<at>gmail[dot]com
*
*/
/*
* Boost Software License - Version 1.0 - August 17th, 2003
*
* Permission is hereby granted, free of charge, to any person or organization
* obtaining a copy of the software and accompanying documentation covered by
* this license (the "Software") to use, reproduce, display, distribute,
* execute, and transmit the Software, and to prepare derivative works of the
* Software, and to permit third-parties to whom the Software is furnished to
* do so, all subject to the following:
*
* The copyright notices in the Software and this entire statement, including
* the above license grant, this restriction and the following disclaimer,
* must be included in all copies of the Software, in whole or in part, and
* all derivative works of the Software, unless such copies or derivative
* works are solely in the form of machine-executable object code generated by
* a source language processor.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

//! Attribute pertinent type and kind global definitions.
/*!
Attribute pertinent type and kind global definitions.
*/

#ifndef DBO_KINDDEFS_H_INCLUDED
#define DBO_KINDDEFS_H_INCLUDED

#include "namespace.h"
#include "typesdef.h"

NAMESPACE_DBO_OPEN

//<! Define all known base kind types.
//<! Do not use enum until c++11 is fully deployed
//<! This is the list of all known attribute/object kinds.
//<! Just add numbers, DO NOT change existing magic numbers.

const   dbo_kind_t DBO_KIND_NULL             = 0x00;

const   dbo_kind_t DBO_KIND_LITTLE_ENDIAN    =    7;
const   dbo_kind_t DBO_KIND_BIG_ENDIAN       =    8;

const   dbo_kind_t DBO_KIND_STRING           =   10;
const   dbo_kind_t DBO_KIND_WSTRING          =   11;
const   dbo_kind_t DBO_KIND_SINGLE           =   12;
const   dbo_kind_t DBO_KIND_DOUBLE           =   13;
const   dbo_kind_t DBO_KIND_DECIMAL          =   14;
const   dbo_kind_t DBO_KIND_INT8             =   15;
const   dbo_kind_t DBO_KIND_UINT8            =   16;
const   dbo_kind_t DBO_KIND_INT16            =   17;
const   dbo_kind_t DBO_KIND_UINT16           =   18;
const   dbo_kind_t DBO_KIND_INT32            =   19;
const   dbo_kind_t DBO_KIND_UINT32           =   20;
const   dbo_kind_t DBO_KIND_INT64            =   21;
const   dbo_kind_t DBO_KIND_UINT64           =   22;
const   dbo_kind_t DBO_KIND_DATETIME         =   23;

const   dbo_kind_t DBO_KIND_LIST_STRING      =  110;
const   dbo_kind_t DBO_KIND_LIST_WSTRING     =  111;
const   dbo_kind_t DBO_KIND_LIST_FLOAT       =  112;
const   dbo_kind_t DBO_KIND_LIST_DOUBLE      =  113;
const   dbo_kind_t DBO_KIND_LIST_DECIMAL     =  114;
const   dbo_kind_t DBO_KIND_LIST_INT8        =  115;
const   dbo_kind_t DBO_KIND_LIST_UINT8       =  116;
const   dbo_kind_t DBO_KIND_LIST_INT16       =  117;
const   dbo_kind_t DBO_KIND_LIST_UINT16      =  118;
const   dbo_kind_t DBO_KIND_LIST_INT32       =  119;
const   dbo_kind_t DBO_KIND_LIST_UINT32      =  120;
const   dbo_kind_t DBO_KIND_LIST_INT64       =  121;
const   dbo_kind_t DBO_KIND_LIST_UINT64      =  122;
const   dbo_kind_t DBO_KIND_LIST_DATETIME    =  123;

const   dbo_kind_t DBO_KIND_OBJECT           =  220;
const   dbo_kind_t DBO_KIND_LIST_OBJECT      =  221;

const   dbo_kind_t DBO_KIND_OBJECT_START     =  222;
const   dbo_kind_t DBO_KIND_OBJECT_END       =  223;

const   dbo_kind_t DBO_KIND_LAST             =  255;

NAMESPACE_DBO_CLOSE

#endif /* !defined(DBO_KINDDEFS_H_INCLUDED) */

